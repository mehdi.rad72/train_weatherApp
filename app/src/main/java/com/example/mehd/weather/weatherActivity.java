package com.example.mehd.weather;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

import static java.lang.Double.*;

public class weatherActivity extends AppCompatActivity {

    TextView city;
    TextView country;
    TextView temp;
    TextView sunrise;
    TextView sunset;
    TextView windspeed;
    EditText cityname;
    String cityvalue;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        dialog = new ProgressDialog(this);
        dialog.setTitle("waiting");
        dialog.setMessage("please wait until data load");

        city= (TextView) findViewById(R.id.city);
        country= (TextView) findViewById(R.id.country);
        temp= (TextView) findViewById(R.id.temp);
        sunrise= (TextView) findViewById(R.id.sunrise);
        sunset= (TextView) findViewById(R.id.sunset);
        windspeed= (TextView) findViewById(R.id.windspeed);
        cityname = (EditText) findViewById(R.id.cityname);

        findViewById(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityvalue = cityname.getText().toString();

                String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + cityvalue
                        + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

            getdata(url);
            }
        });
    }

    private void getdata(String url) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(weatherActivity.this, "conection eror", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                parsData(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }
        });

    }

    private void parsData(String responseString) {

        Gson gson = new Gson();
        YahooClass yahoo = gson.fromJson(responseString, YahooClass.class);
        String Temp = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        String City = yahoo.getQuery().getResults().getChannel().getLocation().getCity();
        String Country = yahoo.getQuery().getResults().getChannel().getLocation().getCountry();
        String Sunrise = yahoo.getQuery().getResults().getChannel().getAstronomy().getSunrise();
        String Sunset = yahoo.getQuery().getResults().getChannel().getAstronomy().getSunset();
        String Windspeed = yahoo.getQuery().getResults().getChannel().getWind().getSpeed();

        Double c = parseDouble(Temp);
        c = (c-32)/1.8;
        String cTemp = String.format("%.2f",c);

        temp.setText( cTemp + "°C");
        country.setText(Country);
        sunrise.setText(Sunrise);
        sunset.setText(Sunset);
        windspeed.setText(Windspeed + " mph");
        city.setText(City);
    }
}
